module.exports = function(folder , ext , callback) {
    var fs = require("fs");
    var path = require("path");

    fs.readdir(folder , function(err , data){
       if ( err ) {
          return callback(err);
       }

       var matches = data.filter(function(e){
           return (path.extname(e) === '.'+ext);
       })

      callback(err , matches);
    });
};