var params = process.argv;
params.shift();
params.shift();

var folder = params[0];
var filter = params[1];

var fs = require('fs');
var path = require('path');

fs.readdir(folder , function(err, data){
   if ( !err ) {
       var matches = data.filter(function(e) {

            return (path.extname(e) === "."+filter);
       });

       console.log(matches.join("\n"));
   }
});

